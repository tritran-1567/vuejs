import Vue from 'vue'
import Router from 'vue-router'
import About from '../pages/about'
import Privacy from '../pages/privacy'
import Faq from '../pages/faq'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/about',
            name: 'about',
            component: About
        },
        {
            path: '/privacy',
            name: 'privacy',
            component: Privacy
        },
        {
            path: '/faq',
            name: 'faq',
            component: Faq
        },
    ]
})