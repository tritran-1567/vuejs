import Vue from 'vue'
import About from './pages/about.vue'
import Faq from './pages/faq.vue'
import Privacy from './pages/privacy.vue'
import Home from './App.vue'

const NotFound = { template: '<p style="position:absolute;top:50%;left:50%;transform:translate(-50%, -50%);font-size:200%;font-weight:bold">Page not found<br/><a href="/"> < Back home</a></p>' }

const routes = {
  '/': Home,
  '/about/': About,
  '/faq/': Faq,
  '/privacy/': Privacy,
}

new Vue({
  el: '#app',
  data: {
    currentRoute: window.location.pathname
  },
  computed: {
    ViewComponent () {
      return routes[this.currentRoute] || NotFound
    }
  },
  render (h) { return h(this.ViewComponent) }
})